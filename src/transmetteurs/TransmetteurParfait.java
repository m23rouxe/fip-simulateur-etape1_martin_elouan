package transmetteurs;

import destinations.DestinationInterface;
import information.Information;
import information.InformationNonConformeException;

import java.lang.reflect.Type;

/**
 * Cette classe représente un transmetteur parfait qui transmet sans erreur l'information reçue à toutes les destinations connectées.
 *
 * Boolean est Le type de l'information transmise.
 */
public class TransmetteurParfait extends Transmetteur<Boolean, Boolean> {

    /**
     * Construit un nouveau transmetteur parfait.
     */
    public TransmetteurParfait() {
        super();
        informationRecue = new Information<>();
        informationEmise = new Information<>();
    }

    /**
     * Reçoit une information et la stocke dans le transmetteur.
     *
     * @param information L'information à recevoir.
     * @throws InformationNonConformeException Si l'information n'est pas conforme.
     */
    public void recevoir(Information<Boolean> information) throws InformationNonConformeException {
        for (Boolean valeur : information) {
            try {
                informationRecue.add(valeur);
            } catch (Exception e) {
                throw new InformationNonConformeException();
            }
        }
    }

    /**
     * Émet l'information stockée dans le transmetteur vers toutes les destinations connectées.
     *
     * @throws InformationNonConformeException Si l'information n'est pas conforme.
     */
    public void emettre() throws InformationNonConformeException {
        informationEmise = informationRecue; // Transmission parfaite
        for (DestinationInterface<Boolean> destination : destinationsConnectees) {
            try {
                destination.recevoir(informationEmise);
            } catch (Exception e) {
                throw new InformationNonConformeException();
            }
        }
    }
}