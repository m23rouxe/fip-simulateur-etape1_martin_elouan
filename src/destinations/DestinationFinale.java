package destinations;

import information.Information;
import information.InformationNonConformeException;

public class DestinationFinale extends Destination<Boolean> {

	public DestinationFinale() {
		informationRecue = new Information<>();
	}

	public void recevoir(Information<Boolean> information) throws InformationNonConformeException {
	
		for(Boolean valeur : information){

			try {
				informationRecue.add(valeur);
			} catch (Exception e) {
				throw new InformationNonConformeException();
			}

		}
		
	}
	
}
