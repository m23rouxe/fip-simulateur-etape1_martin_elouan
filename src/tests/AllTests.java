package tests;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import simulateur.SimulateurTest;

public class AllTests {

    public static void main(String[] args) {
        // Create a JUnitCore instance
        JUnitCore junit = new JUnitCore();

        // Run the tests
        Result result = junit.run(SimulateurTest.class);

        // Get the number of tests run, failed, and ignored
        int runCount = result.getRunCount();
        int failureCount = result.getFailureCount();
        int ignoreCount = result.getIgnoreCount();

        // Print out the results
        System.out.println("Résumé des tests :");
        System.out.println("Nombre total de tests exécutés : " + runCount);
        System.out.println("Nombre de tests échoués : " + failureCount);
        System.out.println("Nombre de tests ignorés : " + ignoreCount);

        // Print detailed information about failed tests
        if (failureCount > 0) {
            System.out.println("\nDétails des échecs :");
            for (Failure failure : result.getFailures()) {
                System.out.println("Test échoué : " + failure.getDescription());
                System.out.println("Exception : " + failure.getException());
                System.out.println();
            }
        }
    }
}
