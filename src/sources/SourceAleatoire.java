package sources;

import information.Information;
import information.InformationNonConformeException;

import java.util.Random;

/**
 * Cette classe représente une source aléatoire générant une séquence de valeurs booléennes aléatoires.
 *
 * Le type de l'information générée par cette source est Boolean.
 */
public class SourceAleatoire extends Source<Boolean> {

    /**
     * Construit une nouvelle source aléatoire avec une graine et un nombre de bits spécifiés.
     *
     * @param seed La graine pour le générateur de nombres aléatoires. Si nulle, l'heure système actuelle est utilisée.
     * @param nbBitsMsg Le nombre de bits à générer.
     */
    public SourceAleatoire(Integer seed, int nbBitsMsg) {
        super();
        informationGeneree = new Information<>();
        Random rand = new Random();
        if (seed == null) rand.setSeed(System.currentTimeMillis());
        else rand.setSeed(seed);

        for (int i = 0; i < nbBitsMsg; i++) {
            informationGeneree.add(rand.nextBoolean());
        }
    }
}