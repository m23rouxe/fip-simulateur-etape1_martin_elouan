package sources;

import information.Information;
import destinations.DestinationInterface;
import java.util.LinkedList;

/**
 * Classe SourceFix qui hérite de la classe abstraite Source
 * et génère et émet un message en utilisant une chaîne de bits donnée.
 */
public class SourceFixe extends Source<Boolean> {

	/**
	 * Construit une nouvelle source fixe avec le message à transmettre.
	 *
	 * @param m Message à émettre avec la source fixe.
	 */
	public SourceFixe(String m) {
		
		super();

		informationGeneree = new Information<Boolean>();

		for(String s : m.split("")) {
			informationGeneree.add(s != "0"); // 0->false & 1->true
		}

	}
}
