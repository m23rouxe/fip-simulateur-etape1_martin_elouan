package simulateur;

import static org.junit.Assert.*;

import information.Information;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

public class SimulateurTest {

    private Simulateur simulateur;
    private static int totalTests = 0;
    private static int failedTests = 0;

    @Rule
    public ErrorCollector collector = new ErrorCollector();

    @Test
    public void testConstructeurAvecArgumentsValides() throws Exception {
        totalTests++;
        System.out.println("Début du test : testConstructeurAvecArgumentsValides");

        String[] args = {"-mess", "1100110", "-seed", "42"};
        simulateur = new Simulateur(args);

        try {
            assertNotNull("La source devrait être initialisée", simulateur.source);
            assertNotNull("Le transmetteur devrait être initialisé", simulateur.transmetteurLogique);
            assertNotNull("La destination devrait être initialisée", simulateur.destination);
        } catch (AssertionError e) {
            collector.addError(e);
            failedTests++;
        } finally {
            System.out.println("Test terminé : testConstructeurAvecArgumentsValides");
        }
    }

    @Test
    public void testConstructeurAvecArgumentsInvalide() throws Exception {
        totalTests++;
        System.out.println("Début du test : testConstructeurAvecArgumentsInvalide");

        String[] args = {"-mess", "1100abc", "-seed", "42"};
        try {
            simulateur = new Simulateur(args);
            fail("Une exception ArgumentsException était attendue");
        } catch (ArgumentsException e) {
            // Exception attendue, test réussi
        } catch (Exception e) {
            collector.addError(e);
            failedTests++;
        } finally {
            System.out.println("Test terminé : testConstructeurAvecArgumentsInvalide");
        }
    }

    @Test
    public void testExecute() throws Exception {
        totalTests++;
        System.out.println("Début du test : testExecute");

        String[] args = {"-mess", "1100", "-seed", "42"};
        simulateur = new Simulateur(args);

        try {
            // Ce test suppose que la méthode execute ne lance pas d'exceptions
            simulateur.execute();

            // Valider le résultat, par exemple, vérifier que la destination a reçu l'information
            assertNotNull("La destination devrait avoir reçu l'information", simulateur.destination.getInformationRecue());
        } catch (AssertionError e) {
            collector.addError(e);
            failedTests++;
        } catch (Exception e) {
            collector.addError(e);
            failedTests++;
        } finally {
            System.out.println("Test terminé : testExecute");
        }
    }

    @Test
    public void testCalculTauxErreurBinaire() throws Exception {
        totalTests++;
        System.out.println("Début du test : testCalculTauxErreurBinaire");

        String[] args = {"-mess", "1100", "-seed", "42"};
        simulateur = new Simulateur(args);
        simulateur.execute();

        try {
            // Définir les valeurs attendues pour le test (c'est un peu artificiel et dépend de l'implémentation)
            Information<Boolean> informationEmise = simulateur.source.getInformationEmise();
            Information<Boolean> informationRecue = simulateur.destination.getInformationRecue();

            // Définir des valeurs connues pour le test
            float expectedTEB = 1.0f; // Cette valeur devrait être basée sur les résultats attendus
            assertEquals(expectedTEB, simulateur.calculTauxErreurBinaire(), 0f);
        } catch (AssertionError e) {
            collector.addError(e);
            failedTests++;
        } catch (Exception e) {
            collector.addError(e);
            failedTests++;
        } finally {
            System.out.println("Test terminé : testCalculTauxErreurBinaire");
        }
    }

    @Test
    public void testAvecArgumentsParDefaut() throws Exception {
        totalTests++;
        System.out.println("Début du test : testAvecArgumentsParDefaut");

        String[] args = {};
        simulateur = new Simulateur(args);

        try {
            // Vérifier si les valeurs par défaut sont correctement définies
            assertTrue("Le message devrait être généré aléatoirement par défaut", simulateur.messageAleatoire);
            assertEquals("Le nombre de bits par défaut devrait être 100", 100, simulateur.nbBitsMess);
        } catch (AssertionError e) {
            collector.addError(e);
            failedTests++;
        } catch (Exception e) {
            collector.addError(e);
            failedTests++;
        } finally {
            System.out.println("Test terminé : testAvecArgumentsParDefaut");
        }
    }
}
