# Simulateur de chaine de transmission

<figure style="margin-left:auto;margin-right:auto;width:80%;text-align:center">
  <img style="border-radius: 15px;" src="readmeAssets/UMLsimpl.png" alt="Diagramme UML de base"><br>
  <caption style="display: block">Figure 1: Diagramme de classes UML fournit au début du projet</caption>
</figure>

L'objet du projet est de créer un simulateur de chaine de transmission en Java (OpenJdk 21). Comme montré en **figure 2**, le simulateur est décomposé en blocs :
- Les sources, qui se connectent à d'autres blocs et génèrent les informations à transmettre.
- Les transmetteurs, qui recoivent des informations de sources, ou d'autres transmetteurs et se connectent à une destination pour leur redonner les informations reçues avec ou sans modification.
- Les destinations, recoivent les données, elles peuvent aussi servir de sondes pour évaluer le fonctionnement du simulateur. 

# Installation/Usage

- Utiliser `./compile` pour compiler
- Utiliser `./simulateur <args>` pour exécuter le simulateur avec des arguments
- Utiliser `./runTests` pour lancer les auto tests

# Project status
## v1.0

**Options à implémenter pour la première version :**

Par défaut le simulateur doit utiliser une chaîne de transmission
logique, avec un message aléatoire de longueur 100, sans utilisation
de sondes et sans utilisation de codeur.

`-mess m`

précise le message ou la longueur du message à émettre :
Si m est une suite de 0 et de 1 de longueur au moins égale à 7, m est
le message à émettre. Si m comporte au plus 6 chiffres décimaux et
correspond à la représentation en base 10 d'un entier, cet entier est
la longueur du message que le simulateur doit générer et transmettre.
Par défaut le simulateur doit générer et transmettre un message de
longueur 100.

`-s`

indique l’utilisation des sondes.
Par défaut le simulateur n’utilise pas de sondes

`-seed v`

précise l’utilisation d’une semence pour l’initialisation des
générateurs aléatoires du simulateur. v doit être une valeur
entière. L’utilisation d’une semence permet de rejouer à l’identique
une simulation (à la fois pour le message émis et le bruitage s’il est
activé). Par défaut le simulateur n’utilise pas de semence pour
initialiser ses générateurs aléatoires.

<figure style="margin-left:auto;margin-right:auto;width:90%;text-align:center">
  <img style="border-radius: 15px;" src="readmeAssets/UMLv1.0.png" alt="Diagramme UML de base">
  <caption style="display: block">Figure 2: Diagramme de classes UML pour la première version</caption>
</figure>

# Contributors
- Martin ROUXEL (m23rouxe)
- Elouan VIVET (e23vivet)
